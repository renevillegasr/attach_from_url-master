from odoo import api, fields, models
from odoo.exceptions import UserError
import urllib2, base64


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def upload_from_buttom(self):
        ref = self.partner_id.ref
        if not ref:
            raise UserError('El proveedor no tiene una Referencia asignada')

        reference = self.reference
        serial = reference.split('-')
        if len(serial) == 1:
            doc = urllib2.urlopen(
                'http://siic.raloy.com.mx/proveedores/php/funciones.php?fase=get_doc&p=' + ref + '&f=' + serial[
                    0] + '&s=' + '').read()
        else:
            doc = urllib2.urlopen(
                'http://siic.raloy.com.mx/proveedores/php/funciones.php?fase=get_doc&p=' + ref + '&f=' + serial[
                    1] + '&s=' + serial[0]).read()

        if doc == 'None':
            raise UserError('No existe documento con esa referencia, Folio y Serie')

        doc = doc.split('@')
        urlxml = doc[0]
        urlpdf = doc[1]

        filexml = base64.encodestring(urllib2.urlopen(
            'http://siic.raloy.com.mx/websiic/visores/finanzas/php/funciones.php?fase=cfds_ver_archivo&uri=' + urlxml).read())
        filepdf = base64.encodestring(urllib2.urlopen(
            'http://siic.raloy.com.mx/websiic/visores/finanzas/php/funciones.php?fase=cfds_ver_archivo&uri=' + urlpdf).read())

        print(self.env['ir.attachment'].create({
            'name': urlxml,  # Puede ser cualquiera
            'res_model': 'account.invoice',
            'res_id': self.id,
            'datas_fname': urlxml,
            'type': 'binary',  # Si no es URL, quitar esta linea
            # 'type': 'url',  # Si no es URL, quitar esta linea
            # 'url': 'http://siic.raloy.com.mx/websiic/visores/finanzas/php/funciones.php?fase=cfds_ver_archivo&uri=' + urlxml,  # Cualquier URL
            'datas': filexml,  # Si no es URL, poner aqui la data en base 64
        }))

        print(self.env['ir.attachment'].create({
            'name': urlpdf,  # Puede ser cualquiera
            'res_model': 'account.invoice',
            'res_id': self.id,
            'datas_fname': urlpdf,
            'type': 'binary',  # Si no es URL, quitar esta linea
            # 'type': 'url',  # Si no es URL, quitar esta linea
            # 'url': 'http://siic.raloy.com.mx/websiic/visores/finanzas/php/funciones.php?fase=cfds_ver_archivo&uri=' + urlpdf,  # Cualquier URL
            'datas': filepdf,  # Si no es URL, poner aqui la data en base 64
        }))
